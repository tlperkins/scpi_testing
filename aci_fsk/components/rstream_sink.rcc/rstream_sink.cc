/*
 * THIS FILE WAS ORIGINALLY GENERATED ON Thu Sep  8 10:53:09 2022 CDT
 * BASED ON THE FILE: rstream_sink.xml
 * YOU *ARE* EXPECTED TO EDIT IT
 *
 * This file contains the implementation skeleton for the rstream_sink worker in C++
 */

#include "rstream_sink-worker.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Rstream_sinkWorkerTypes;

class Rstream_sinkWorker : public Rstream_sinkWorkerBase {

  RCCResult run(bool /*timedout*/) {
  
  const int16_t *inData = in.data().real().data();
  const size_t num_of_elements = in.data().real().size();

  for (unsigned int i = 0; i < num_of_elements; i++){
      inData++;
  }

    return RCC_ADVANCE; // change this as needed for this worker to do something useful
    // return RCC_ADVANCE; when all inputs/outputs should be advanced each time "run" is called.
    // return RCC_ADVANCE_DONE; when all inputs/outputs should be advanced, and there is nothing more to do.
    // return RCC_DONE; when there is nothing more to do, and inputs/outputs do not need to be advanced.
  }
};

RSTREAM_SINK_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in any case
RSTREAM_SINK_END_INFO
