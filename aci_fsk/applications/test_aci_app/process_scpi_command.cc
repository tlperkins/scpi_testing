#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <time.h>
#include <arpa/inet.h>
#include <iostream>

#define MAXLINE 1024

int process_scpi_command(int sock, char *scpi_cmd, int cmd_len, char * scpi_reply){
	
    float num_seconds = 1;
    int micro_seconds = (int)(1000000 * num_seconds);
    //char * scpi_response = (char*) malloc(100); 
    int len_scpi_reply;

    // Send SCPI Command to instrument
         send(sock, scpi_cmd, cmd_len, 0);
    	 char buf_cr[1] = {0x0D};
   	 send(sock,buf_cr, 1, 0);

    //  Wait after send
    	clock_t start_time = clock();
    	while( clock() < start_time + micro_seconds);

    // Receive reply
    
        char buffer[MAXLINE];
        memset(buffer, 0, sizeof(buffer));
   	int recv_len = recv(sock, buffer,MAXLINE,0);
        //printf("recv_len = %d\n",recv);
        //printf("\r\rBuffer: ");
    	//puts(buffer);
	//printf("\r\r");

         for (int i = 0; i < 45; i++){
		scpi_reply[i] = buffer[i + cmd_len + 2];
		//scpi_response[i] = buffer[i + cmd_len + 2];
	}

	//printf("Length of scpi_response %d\n",strlen(scpi_response));

	//len_scpi_reply = strlen(scpi_response);
	//for (int i = 0; i < (len_scpi_reply - 6); i++){
	//	scpi_reply[i] = scpi_response[i];	
	//}
        //printf("\rLength of scpi_reply: %d\r",strlen(scpi_reply));

        //std::cout << "response: " << scpi_reply << std::endl;
        //printf("Response legnth = %d\n",recv_len);
	//strncpy(scpi_reply,buffer,recv_len);
	//scpi_reply[12]='\0';
	//free(scpi_response);

	return strlen(scpi_reply);
} 
