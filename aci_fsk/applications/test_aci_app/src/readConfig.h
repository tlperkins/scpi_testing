#include <iostream>

struct SCPI_CONFIG{
	std::string scpi_host;
	std::string scpi_port;
};


int readConfig(std::string& configFile, SCPI_CONFIG *scpiConfig);
