#include <stdio.h>
#include <iostream>
#include <string>
#include <unistd.h>

#include "readConfig.h"

using namespace std;

int main()
{
	string configFile = "./cfg/scpi_interface.cfg";
	SCPI_CONFIG *scpiConfig = new SCPI_CONFIG;

	int result = readConfig(configFile,scpiConfig);

	cout << "host = " << scpiConfig->scpi_host << endl;


	return 0;
}
