
#include <fstream>      //ifstream
#include <iostream>     //cerr
#include <stdlib.h>     //exit(0);
#include <string.h>       //string, getline
#include <unistd.h>
#include <vector>
#include <signal.h>     //SIGINT
#include <ctype.h>
//#include "stdafx.h"
#include "readConfig.h"

char* trim(char* input);

int readConfig(std::string& configFile, SCPI_CONFIG *scpiConfig){

	std::cout << "FILE: " << configFile << std::endl;
	std::ifstream file(configFile.c_str());
	std::string line;
	std::string delimiter = "=";

	if(!file.good()){
		std::cerr << "ERROR - unable to open config file " + configFile <<std::endl;
		exit(1);
	}

 	while(std::getline(file, line)){
		if(!line.empty()){
			size_t delimiterLocation =line.find(delimiter);
			if(delimiterLocation == line.length()){
                		std::cerr << "Error in configuration file: " << configFile.c_str() << "; errored line reads: " << line.c_str() << std::endl;
                		std::cerr << "Line requires '=' character as delimiter to key value pair, skipping line and continuing to read!" << std::endl;
                		continue;
           		}

			//Acquire given configuration key
			std::string cfgKey = line.substr(0, delimiterLocation);
			
			//std::cout << "cfgkey: " << cfgKey << std::endl;
            		/*cfgKey.erase(std::remove(cfgKey.begin(), cfgKey.end(), isspace), cfgKey.end());
            		if(cfgKey.length() == 0){
                		std::cerr << "Error in configuration file: " << configFile.c_str() << "; errored line reads: " << line.c_str() << std::endl;
                		std::cerr << "Key required before '=' delimiter, skipping line and continuing to read!" << std::endl;
                		continue;
            		}*/

			//Acquire given configurqation value
			std::string cfgValue = line.substr(delimiterLocation + delimiter.length(), line.length());
            		//std::cout << "cfgValue: " << cfgValue << std::endl;
			/*cfgValue.erase(std::remove(cfgValue.begin(), cfgValue.end(), isspace), cfgValue.end());
            		if(cfgValue.length() == 0){
                		std::cerr << "Error in configuration file: " << configFile.c_str() << "; errored line reads: " <<  line.c_str() << std::endl;
               			std::cerr << "Value required after '=' delimiter, skipping line and continuing to read!" << std::endl;
               	 		continue;
            		}*/
			if(cfgKey == "scpi_host"){
				//std::cout << "value = " << cfgValue << std::endl;
				scpiConfig->scpi_host = cfgValue;
				}
			else if(cfgKey == "scpi_port"){
				//std::cout << "value = " << cfgValue << std::endl;
				scpiConfig->scpi_port = cfgValue;
				}
			else {
				std::cout << " cfgValue not found!" << std::endl;
				}
	
		}
	}

	return 0;
}


char *ltrim(char *s) 
{     
    while(isspace(*s)) s++;     
    return s; 
}  

char *rtrim(char *s) 
{     
    char* back;
    int len = strlen(s);

    if(len == 0)
        return(s); 

    back = s + len;     
    while(isspace(*--back));     
    *(back+1) = '\0';     
    return s; 
}  

char *trim(char *s) 
{     
    return rtrim(ltrim(s));  
} 
