/* http://l3net.wordpress.com/2012/12/09/a-simple-telnet-client/ */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <arpa/inet.h>
#include <termios.h>
#include <fcntl.h>
#include <time.h>
#include "readConfig.h"
#include "processSCPI.h" 


#define DO 0xfd
#define WONT 0xfc
#define WILL 0xfb
#define DONT 0xfe
#define CMD 0xff
#define CMD_ECHO 1
#define CMD_WINDOW_SIZE 31

using namespace std;
 

static struct termios tin;
 
static void terminal_set(void) {
    // save terminal configuration
    tcgetattr(STDIN_FILENO, &tin);
     
    static struct termios tlocal;
    memcpy(&tlocal, &tin, sizeof(tin));
    cfmakeraw(&tlocal);
    tcsetattr(STDIN_FILENO,TCSANOW,&tlocal);
}
 
static void terminal_reset(void) {
    // restore terminal upon exit
    tcsetattr(STDIN_FILENO,TCSANOW,&tin);
}
 
#define BUFLEN 20
#define MAXLINE 1024
int main(int argc , char *argv[]) {


    unsigned char buf[BUFLEN + 1];
    int n, len;
    float num_seconds = 0.1;
    int micro_seconds = (int)(1000000 * num_seconds);

    // Read in Intrument parameters
    string configFile="./cfg/scpi_interface.cfg";
    SCPI_CONFIG *scpiConfig = new SCPI_CONFIG;
    int result = readConfig(configFile, scpiConfig);
 
    //Create socket
    int sock;
    struct sockaddr_in server;

    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1) {
        perror("Could not create socket. Error");
        return 1;
    }
 
    server.sin_addr.s_addr = inet_addr(scpiConfig->scpi_host.c_str());
    server.sin_family = AF_INET;
    server.sin_port = htons(atoi(scpiConfig->scpi_port.c_str()));
 
    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0) {
        perror("connect failed. Error");
        return 1;
    }
    puts("Connected...\n");
 
    // set terminal
    terminal_set();
    atexit(terminal_reset);

    // Initialize Telent	
    result = init_scpi_interface(sock);

   // while(1) {
    	//printf("\r<Enter>");
    	char whatever; /* = getc(stdin);*/
    	//printf("\n");

    	//if (whatever == 'X'){
	//	printf("\n\r");
        //	return 0;
	//}

    	char scpi_command[100];// = "*IDN?";i
        char peak[100];
    	char * scpi_reply = (char*) malloc(100); 
    	int scpi_len = 5;
    	strcpy(scpi_command,"*IDN?");
  // printf("message size: %d\n",sizeof(scpi_command));

    	int response = process_scpi_command(sock, scpi_command, scpi_len, scpi_reply);
    	printf("\r%s\n",scpi_reply);

while(1){	
    	//printf("\r<Start SCPI>");
    	whatever = getc(stdin);
    	printf("\n");

    	if (whatever == 'X'){
		printf("\n\r");
        	return 0;
	}

    	scpi_len = 18;
    	strcpy(scpi_command,"CALC:MARK:FUNC:MAX");

    	response = process_scpi_command(sock, scpi_command, scpi_len, scpi_reply);
    	//printf("\rRETURNED 1 = %s \n Len = %d",scpi_reply,response);
	
    	//printf("\r<Enter>");
    	//whatever = getc(stdin);
    	//printf("\n");

    	scpi_len = 13;
    	strcpy(scpi_command,"CALC:MARK1:X?");

    	response = process_scpi_command(sock, scpi_command, scpi_len, scpi_reply);
	//for (int i = 0; i < response-10; i++){
	//	peak[i] = scpi_reply[i];
	//}

    	printf("\rPeak: ");
	for (int i = 0; i < 16; i++)  printf("%c",scpi_reply[i]);
	printf("\n\n");
}

    	free(scpi_reply);
//}
    /*printf("\rSending Message...\n");

    //sendto(sock, (const char*)message, strlen(message),0, (const struct sockaddr*)&server, sizeof(server));
    
    char buffer[MAXLINE];
    memset(buffer, 0, sizeof(buffer));  
    strcpy(buffer,message);
    send(sock, "*IDN?", 5, 0);
    char buf_cr[1] = {0x0D};
    send(sock,buf_cr, 1, 0);  

    clock_t start_time = clock();  
    while( clock() < start_time + micro_seconds);

    printf("\rReceiving Message...\n");

   int recv_len = recv(sock, buffer,MAXLINE,0);
   printf("\rReceived message size = %d\n",recv_len);
   printf("\r");
//n = recvfrom(sock, (char*)buffer, MAXLINE, 0, (struct sockaddr*)&server, sizeof(server));
    puts(buffer);


    /*while(1){
	std::cout << " Processing SCPI..." << std::endl;
	result = processScpiCommand(sock,false);
	if(result == 2) {
		std::cout << "Returned 2" << std::endl;
		break;
	}
    }*/
//}
    close(sock);

  // printf("\n\r");
    return 0;
}
