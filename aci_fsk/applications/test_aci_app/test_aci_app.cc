#include <chrono>
#include <thread>
#include <iostream>
#include <string.h>
#include <stdlib.h>     //for using the function sleep
#include <unistd.h>
#include <arpa/inet.h>
#include <termios.h>
#include <fcntl.h>
#include <stdio.h>
#include <cstdint>
#include <cstring>
#include <time.h>
#include "OcpiApi.hh"
#include "processSCPI.h"

#define DO 0xfd
#define WONT 0xfc
#define WILL 0xfb
#define DONT 0xfe
#define CMD 0xff
#define CMD_ECHO 1
#define CMD_WINDOW_SIZE 31
#define BUFLEN 1024
#define SCPI true

namespace OA = OCPI::API;

// Sleep for micros microseconds
   void sleep(const int micros) {
       std::this_thread::sleep_for(std::chrono::microseconds(micros));
   }



static struct termios tin;

static void terminal_set(void) {
    // save terminal configuration
    tcgetattr(STDIN_FILENO, &tin);
    static struct termios tlocal;
    memcpy(&tlocal, &tin, sizeof(tin));
    cfmakeraw(&tlocal);
    tcsetattr(STDIN_FILENO,TCSANOW,&tlocal);
}


static void terminal_reset(void) {
    // restore terminal upon exit
    tcsetattr(STDIN_FILENO,TCSANOW,&tin);
}
    

int main(/*int argc, char **argv*/) {


    printf("*************************************\n");
    printf("*  Starting combined ACI and SCPI   *\n");
    printf("*************************************\n");


/* Establish Telnet COnnection to Instrument */
   int sock;
    struct sockaddr_in server;
    unsigned char buffer[BUFLEN + 1];

    int port = 5024;  //SCPI Telent Port
    char ip_addr[] = "192.168.1.18";

    float num_seconds = 1;
    int micro_seconds = (int)(1000000 * num_seconds);

    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1) {
        perror("Could not create socket. Error");
        return 1;
    }

    server.sin_addr.s_addr = inet_addr(ip_addr);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
   //Connect to remote server
if(SCPI){ 
  if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0) {
         perror("connect failed. Error");
         return 1;
   }
   puts("\nConnected...\n");
   
  // set terminal
  terminal_set();
  atexit(terminal_reset);

  int result = init_scpi_interface(sock);

  printf("\r");
}
//  unsigned char scpi_cmd[7]="*IDN?\n";

//  for (int i = 0; i < 6; i++){
//	printf("%c",scpi_cmd[i]);
//  }

//	int scpi_response = send(sock,scpi_cmd,6,0);
//        clock_t start_time = clock();

//	while( clock() < start_time + micro_seconds);

//        int recv_len = recv(sock,buffer,BUFLEN,0);
	//puts(buffer);
//	std::cout << buffer << std::endl;
//	printf("\r");	

//*********************************
// Start OpenCPI Application
// ********************************
  try {
    OA::Application app("../fsk_drc/fsk_drc.xml");
    app.initialize(); // all resources have been allocated
    app.start();      // execution is started
    
    OA:: Property start(app,"drc","start"), stop(app,"drc","stop"), prepare(app,"drc","prepare");
    OA::Property configs(app,"drc","configurations");
    // Do work here.
    //double newValue = 1000;
    //	configs.setValue(newValue,{0,"channels",1,"tuning_freq_MHz"});

   /*
   double rx_value, tx_value;
   rx_value = configs.getValue<double>({0,"channels",0,"tuning_freq_MHz"});
   std::cout << "Rx Frequency: " << rx_value << " MHz" << std::endl;
   tx_value = configs.getValue<double>({0,"channels",1,"tuning_freq_MHz"});
   std::cout << "Tx Frequency: " << tx_value << " MHz" << std::endl;
   */

   double freq_step  = 100;   // MHz
   int time_step     = 1;    // Seconds
   double tx_value   = 2000;  // MHz
   double value;

   char scpi_command[100];
   char peak[100];

   int scpi_len, response;
   char * scpi_reply = (char*) malloc(100);
   
   stop.setValue(0);   

   for (int i = 0; i < 6; i++){
        configs.setValue(tx_value,{0,"channels",1,"tuning_freq_MHz"});
        value = configs.getValue<double>({0,"channels",1,"tuning_freq_MHz"});
        printf("\r");
        std::cout << "tx frequency: " << value << " MHz" << std::endl;
        prepare.setValue(0);
        printf("\r");       
        std::cout << "Starting Tx";
        printf("\r");
        start.setValue(0);

       app.wait(1000000);

        scpi_len = 18;
        strcpy(scpi_command,"CALC:MARK:FUNC:MAX");
        response = process_scpi_command(sock, scpi_command, scpi_len, scpi_reply);
        scpi_len = 13;
        strcpy(scpi_command,"CALC:MARK1:X?");
        response = process_scpi_command(sock, scpi_command, scpi_len, scpi_reply);
        printf("\rPeak: ");
        for (int i = 0; i < 16; i++)  printf("%c",scpi_reply[i]);
        printf("\n\n");

        app.wait(time_step * 1000000);

       stop.setValue(0);   

       tx_value = tx_value + freq_step;
   }

   free(scpi_reply);

    // Must use either wait()/finish() or stop(). The finish() method must
    // always be called after wait(). The start() method can be called
    // again after stop().
    //app.wait();       // wait until app is "done"
/*    std::cout << "press any key to exit..." << std::endl;
    std::cin.get();

    std::cout << " Wait for 10 seconds. " << std::endl;
    for ( int i = 0; i < 10; i++){
    	sleep(1000000);
	std::cout << "Countdown:" << 30-i << std::endl;
   }
*/
    app.finish();     // do end-of-run processing like dump properties
    //app.stop();

  } catch (std::string &e) {
    std::cerr << "app failed: " << e << std::endl;
    return 1;
  }
  return 0;
}
