#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <termios.h>
#include <fcntl.h>
#include "readConfig.h"
#include "processSCPI.h"



#define DO 0xfd
#define WONT 0xfc
#define WILL 0xfb
#define DONT 0xfe
#define CMD 0xff
#define CMD_ECHO 1
#define CMD_WINDOW_SIZE 31 
#define BUFLEN 20


void negotiate(int sock, unsigned char *buf, int len) {
    //std::cout << "\n\rTelnet Negotiation. " << std::endl;

    if (buf[1] == DO && buf[2] == CMD_WINDOW_SIZE) {
        unsigned char tmp1[10] = {255, 251, 31};
        if (send(sock, tmp1, 3 , 0) < 0)
            exit(1);

        unsigned char tmp2[10] = {255, 250, 31, 0, 80, 0, 24, 255, 240};
        if (send(sock, tmp2, 9, 0) < 0)
            exit(1);
        return;
    }

    for (int i = 0; i < len; i++) {
        if (buf[i] == DO)
            buf[i] = WONT;
        else if (buf[i] == WILL)
            buf[i] = DO;
    }

    if (send(sock, buf, len , 0) < 0)
        exit(1);
    
}

int init_scpi_interface(int sock){ 
 struct timeval ts;
    ts.tv_sec = 1; // 1 second
    ts.tv_usec = 0;

//std::cout << "Processing SCPI Command" << std::endl;
    unsigned char buf[BUFLEN + 1];
    int len;
    int cmd_cnt;
    char cmdbuf[] = "*IDN?";
    int cmd_len = sizeof(cmdbuf);
    int recv_cnt = 0;
    char recv_buf[100] = "";
    bool end_recv = false;
    bool status = false;
    int count = 0;

 while (1) {
        // select setup
        fd_set fds;
        FD_ZERO(&fds);
        if (sock != 0) FD_SET(sock, &fds);
        FD_SET(0, &fds);

        // wait for data
        int nready = select(sock + 1, &fds, (fd_set *) 0, (fd_set *) 0, &ts);

        if( count==56 ) return 0;

        if (nready < 0) {
            perror("select. Error");
            return 1;
        }
        else if (nready == 0) {
            ts.tv_sec = 1; // 1 second
            ts.tv_usec = 0;
            printf("\n");
        }
        else if (sock != 0 && FD_ISSET(sock, &fds)) {
            // start by reading a single byte
            int rv;
            if ((rv = recv(sock , buf , 1 , 0)) < 0)
                return 1;
            else if (rv == 0) {
                printf("Connection closed by the remote end\n\r");
                return 0;
            }

            if (buf[0] == CMD) {
                // read 2 more bytes
                len = recv(sock , buf + 1 , 2 , 0);
                if (len  < 0)
                    return 1;
                else if (len == 0) {
                    printf("Connection closed by the remote end\n\r");
                    return 0;
                }
                negotiate(sock, buf, 3);
                //return(0);
            }
            else {
                len = 1;
                recv_buf[recv_cnt] = buf[0];
                recv_cnt++;
                       
                buf[len] = '\0';
                printf("%s", buf);
                count++;
               // printf("count = %d\n",count);
                status = true;
                fflush(0);
            }
        }

        else if (FD_ISSET(0, &fds)) {
                //printf("\r Before getc()\n");
		if(true) return 0;
                //printf("%s",recv_buf);
            	//buf[0] = getc(stdin); //fgets(buf, 1, stdin);

                if(buf[0] == 'X'){
                        printf("\n\r");
			return 2;
                }
                else{
		       for (int j = 0; j <= cmd_len; j++){

				buf[0] = cmdbuf[j];
				
 				if (j == cmd_len){
				 	buf[0] = 0x0D;
				}
			
                        	if (send(sock, buf, 1, 0) < 0) return 1;
                        
                        	if (buf[0] == 0x0D) printf("\n");
			}
                }
        }
    }
    return 0;
}
